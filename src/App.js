import "./App.css";
import { Component } from "react";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      show: true,
      showH1: true,
      showDiv: true,
      showH2: true,
      ShowH3: true,
      showB: true,
    };
  }

  hideButton = () => {
    this.setState({ show: false });
  };
  showButton = () => {
    this.setState({ show: true });
  };
  hideButtonDois = () => {
    this.setState({ showH1: false });
  };
  showButtonDois = () => {
    this.setState({ showH1: true });
  };
  hideButtonTres = () => {
    this.setState({ showDiv: false });
  };
  showButtonTres = () => {
    this.setState({ showDiv: true });
  };
  hideButtonQuatro = () => {
    this.setState({ showH2: false });
  };
  showButtonQuatro = () => {
    this.setState({ showH2: true });
  };
  ButtonCinco = () => {
    this.setState({ showH3: !this.state.showH3 });
  };
  ButtonSeis = () => {
    this.setState({ showB: !this.state.showB });
  };

  render() {
    return (
      <>
        <div>
          <button onClick={this.hideButton}>Esconda Div!</button>
          <button onClick={this.showButton}>mostrar Div!</button>
          {this.state.show && <p>Olá React!</p>}
        </div>
        <div>
          <button onClick={this.hideButtonDois}>Esconda H1!</button>
          <button onClick={this.showButtonDois}>mostrar H1!</button>
          {this.state.showH1 && <h1>Olá, h1</h1>}
        </div>
        <div>
          <button onClick={this.hideButtonTres}>Esconda Div!</button>
          <button onClick={this.showButtonTres}>mostrar Div!</button>
          {this.state.showDiv && <div>Olá, div</div>}
        </div>
        <div>
          <button onClick={this.hideButtonQuatro}>Esconda H2!</button>
          <button onClick={this.showButtonQuatro}>mostrar H2!</button>
          {this.state.showH2 && <h2>Olá, H2</h2>}
        </div>
        <div>
          <button onClick={this.ButtonCinco}>Mostra/Esconde H3!</button>
          {this.state.showH3 && <h3>Olá, H3</h3>}
        </div>
        <div>
          <button onClick={this.ButtonSeis}>Mostra/Esconde B!</button>
          {this.state.showB && <b>Olá, B</b>}
        </div>
      </>
    );
  }
}
export default App;
